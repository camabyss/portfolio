import math,output
reload(output)
screen = []
def final_out():
    global screen
    newlist = sorted(screen, key=lambda k: k['depth'])
    newlist.reverse()
    for shape in newlist:
        if 'points' in shape:
            try:
                #print shape['colours']
                if 'object' in shape:
                    #print 'here'
                    tag = shape['object']
                    #print tag
                    polygon = shape['canvas'].create_polygon(shape['points'],outline=shape['colours'][1],fill=shape['colours'][0],activefill=shape['colours'][1],tags=tag)#,comand=testpolygon)
                    callback = lambda event, tag=tag: output.set_select(event,tag)
                    shape['canvas'].tag_bind(tag,'<Button-1>',callback)
                    #print tag
                else:
                    #print 'no tag'
                    polygon = shape['canvas'].create_polygon(shape['points'],outline=shape['colours'][1],fill=shape['colours'][0],activefill=shape['colours'][1])#,comand=testpolygon)
            except:
                pass
        elif 'char' in shape:
            size = int(shape['size']*100/shape['pos'][2])
            if size > 0 and size<100:
                try:
                    shape['canvas'].create_text(shape['pos'][0],shape['pos'][1],text=shape['char'],font='Arial '+str(size))#,outline='#66f',fill='#9999ff'),height=shape['height']
                except:
                    pass
    screen = []
def drawLine(a,b,canvas,midpoint,Vangle,Vdist,colour='#000'):
    Vsize = [int(canvas['width']),int(canvas['height'])]
    point1 = projection(a,midpoint,Vsize,Vangle,Vdist)
    point2 = projection(b,midpoint,Vsize,Vangle,Vdist)
    try:
        canvas.create_line(point1[0],point1[1],point2[0],point2[1],fill=colour)
    except:
        pass
def drawPlane(points,canvas,midpoint,Vangle,Vdist):
#    Vsize = [int(canvas['width']),int(canvas['height'])]
#    Points = []
#    depths = []
#    for point in points:
#        Point = projection(point,midpoint,Vsize,Vangle,Vdist)
#        Points.append(Point[0])
#        Points.append(Point[1])
#        depths.append(Point[2])
#    #pOints = [10,20,30,40,50,50]
#    depth = 0
#    try:
#        canvas.create_polygon(Points,outline='#f00',fill='#0f0')
#        for d in depths:
#            if d>depth:
#                depth = d
#        return depth        
#    except:
#        pass
    pass
def drawShape (points,sides,canvas,midpoint,Vangle,Vdist):
    #print sides,points
    Vsize = [int(canvas['width']),int(canvas['height'])]
    planes = []
    for side in sides:
        depths = []
        plane = {'points':[],'depth':0}
        for point in side:
            #print points[point],side
            Points = projection(points[point],midpoint,Vsize,Vangle,Vdist)
            if Points != [None,None,None]:
                depths.append(Points[2])
                plane['points'].append([Points[0],Points[1]])
        Depths = 0
        Depth = 0
        for depth in depths:
            Depths += depth
            if depth > Depth:
                Depth = depth
        if Depths != 0:
            plane['depth'] = (Depth/100.00)+(Depths/len(depths))
            plane['canvas'] = canvas
            planes.append(plane)
    return planes
def projection(point,midpoint,Vsize,Vangle,Vdist):
    fail = [None,None,None]
    Xd = point[0]-midpoint[0];
    Yd = point[1]-midpoint[1];
    Zd = point[2]-midpoint[2];
    Vwidth = (Xd*math.cos(Vangle[1]))-(Zd*math.sin(Vangle[1]));
    Vheight = (Yd*math.cos(Vangle[0]))-(Xd*math.sin(Vangle[1])*math.sin(Vangle[0]))-(Zd*math.cos(Vangle[1])*math.sin(Vangle[0]));
    Vdepth = Vdist+((Yd*math.sin(Vangle[0]))+(Xd*math.sin(Vangle[1])*math.cos(Vangle[0]))+(Zd*math.cos(Vangle[1])*math.cos(Vangle[0])));
    if Vsize == 'rotation':
        return [Vwidth,Vheight,Vdepth]
    if Vdepth > 0:
        Vwidthzeta = (math.cos(Vangle[2])*Vwidth)+(math.sin(Vangle[2])*Vheight)
        Vheightzeta = (math.cos(Vangle[2])*Vheight)-(math.sin(Vangle[2])*Vwidth)
        projw = (Vsize[0]/2)-Vwidthzeta*((Vdist-Vsize[0])/Vdepth)
        projh = (Vsize[1]/2)-Vheightzeta*((Vdist-Vsize[1])/Vdepth)
        return [projw, projh, Vdepth]
    else:
        return fail
def rotate(point,origi,rotat):
    proj = projection(point,origi,'rotation',rotat,0)
    if proj != [None,None,None]:
        return [proj[0]+origi[0],proj[1]+origi[1],proj[2]+origi[2]]
    else:
        return point
def draw (drawing,canvas,midpoint,Vangle,Vdist):
    Type = drawing['type']
    cent = drawing['center']
    size = drawing['size']
    try:
        rotations = drawing['rotations']
    except KeyError:
        rotation = [0,0,0]
        rotations = []
    if Type == 'label':
        Vsize = [int(canvas['width']),int(canvas['height'])]
        #for rotation in rotations:
            #cent = rotate(cent,rotation['origin'],rotation['angle'])
        d = cent
        for rotation in rotations:
            d = rotate(d,rotation['origin'],rotation['angle'])
        point = projection(d,midpoint,Vsize,Vangle,Vdist)
        if point != [None,None,None]:
            screen.append({
                'pos':point,#[((projp[0][0]+projp[1][0])/2),((projp[0][1]+projp[2][1])/2)],
                'char':drawing['text'],
                'depth':0,#point[2],
                'canvas':canvas,
                'size':size[0]})
        #canvas.create_text(point[0],point[1],text=drawing['text'])
        #point = projection(drawing['center'],midpoint,Vsize,Vangle,Vdist)
        #points = []
        #for t in range(len(drawing['text'])):
        #    points = [
        #        [cent[0]+(t*1),cent[1],cent[2]],
        #        [cent[0]+(t*1),cent[1]+1,cent[2]],
        #        [cent[0]+((t+1)*1),cent[1],cent[2]],
        #        [cent[0]+((t+1)*1),cent[1]+1,cent[2]]
        #    ]
        #    projp = []
        #    for point in points:
        #        for rotation in rotations:
        #            point = rotate(point,rotation['origin'],rotation['angle'])
        #        projp.append(projection(point,midpoint,Vsize,Vangle,Vdist))
        #    #print projp
        #    if projp[0] != [None,None,None] and projp[1] != [None,None,None] and projp[2] != [None,None,None] and projp[3] != [None,None,None]:
        #        h = ((projp[1][1]+projp[3][1])/2)-((projp[0][1]+projp[2][1])/2)
        #        w = ((projp[2][0]+projp[3][0])/2)-((projp[0][0]+projp[1][0])/2)
        #        screen.append({
        #            'pos':[((projp[0][0]+projp[1][0])/2),((projp[0][1]+projp[2][1])/2)],
        #            'char':drawing['text'][t:t+1],
        #            'height':h,
        #            'width':w,
        #            'depth':0,
        #            'canvas':canvas})
        pass
    elif Type == 'axes':
        point1 = [cent[0]+size,cent[1],cent[2]]
        point2 = [cent[0]-size,cent[1],cent[2]]
        point3 = [cent[0],cent[1]+size,cent[2]]
        point4 = [cent[0],cent[1]-size,cent[2]]
        point5 = [cent[0],cent[1],cent[2]+size]
        point6 = [cent[0],cent[1],cent[2]-size]
        drawLine(point1,point2,canvas,midpoint,Vangle,Vdist)
        drawLine(point3,point4,canvas,midpoint,Vangle,Vdist)
        drawLine(point5,point6,canvas,midpoint,Vangle,Vdist)
    elif Type == '3Darrow':
        point1 = cent
        point2 = [cent[0]+size[0],cent[1]+size[1],cent[2]+size[2]]
        point3 = [cent[0],cent[1]+size[1],cent[2]+size[2]]
        point4 = [cent[0]+size[0],cent[1],cent[2]+size[2]]
        point5 = [cent[0]+size[0],cent[1]+size[1],cent[2]]
        drawLine(point1,point2)
        drawLine(point2,point3)
        drawLine(point2,point4)
        drawLine(point2,point5)
    elif Type == "sphere" :
        sEcts = size[1]
        pi = math.pi
        points = []
        for a in range(sEcts+1):
            th = ((a)/float(sEcts))*pi#-(sEcts/2.00)
            for b in range(2*sEcts):
                ph = (b/float(sEcts))*pi
                d=[cent[0]+size[0]+(size[0]*math.cos(ph)*math.sin(th)),cent[1]+size[0]+(size[0]*math.sin(ph)*math.sin(th)),cent[2]+size[0]+(size[0]*math.cos(th))]
                for rotation in rotations:
                    d = rotate(d,rotation['origin'],rotation['angle'])
                points.append(d)
        sides = make_pattern([0,(pow(sEcts,2)*2)-1,1],(sEcts*2),1)
        planes = drawShape(points,sides,canvas,midpoint,Vangle,Vdist)
        for plane in planes:
            if 'colours' in drawing:
                plane['colours'] = drawing['colours']
            if 'object' in drawing:
                plane['object'] = drawing['object']
            screen.append(plane)
    elif Type == 'cuboid':
        points = []
        for a in range(0,2):
            for b in range(0,2):
                for c in range(0,2):
                    d = [(a*size[0])+cent[0],(b*size[1])+cent[1],(c*size[2])+cent[2]]
                    for rotation in rotations:
                        d = rotate(d,rotation['origin'],rotation['angle'])#[0.6,0.2]#-(size[2]/2.0)
                    points.append(d)
        sides = make_pattern([4,2,1],[1,1,2],[2,4,4])
        planes = drawShape(points,sides,canvas,midpoint,Vangle,Vdist)
        for plane in planes:
            if 'colours' in drawing:
                plane['colours'] = drawing['colours']
            if 'object' in drawing:
                plane['object'] = drawing['object']
            screen.append(plane)
    elif Type == 'grid':
        #print size
        x_w = cent[0]
        z_d = cent[2]
        #print size['block']
        for block in size['blocks']:
            y_h = cent[1]
            #print 'b',block
            for row in range(0,size['rows']):
                #print 'r',row
                center = [x_w,y_h,z_d]
                Size = [(size['block'][0]*block)-0.1,size['block'][1]-0.1,size['block'][2]]
                draw ({
                    'type':'cuboid',
                    'center':center,
                    'size':Size,
                    'rotations':rotations,
                    'colours':drawing['colours'],
                    'object':drawing['object']
                    },
                    canvas,midpoint,Vangle,Vdist)
                y_h += size['block'][1]
            x_w += block*size['block'][0]
####

def make_pattern(start,flip,second):
    pattern = []
    if type(flip) == list:
        for s in range(0,len(start)):
            for switch in range(0,2):
                a = switch * start[s]
                b = a + flip[s]
                c = b + second[s]
                d = c - flip[s]
                side = [a,b,c,d]
                pattern.append(side)
    else:
        for s in range(start[0],start[1],start[2]):
            a = s
            b = a+flip
            c = b+second
            d = c-flip
            side = [a,b,c,d]
            pattern.append(side)
    return pattern