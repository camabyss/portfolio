# -*- coding: utf-8 -*-
import math
import random
from Tkinter import*
#Tkinter._test()
variables = {'sDimensions':3,'infinity':500,'t':0,'fineStructure':1,'view':'mid'}
root = Tk()
root.title("physics")
root.geometry(str(100+variables['infinity']+300)+'x'+str(variables['infinity'])+'+66+20')
canv = Canvas(master=root,width=variables['infinity'],height=variables['infinity'],bg='#ffffff')
syst = Canvas(master=root,width=100,height=variables['infinity'],bg='#ffffff')
tabl = Canvas(master=root,width=300,height=variables['infinity'],bg='#ffffff')
syst.pack(side=LEFT)
canv.pack(side=LEFT)
tabl.pack(side=RIGHT)
Center = [variables['infinity']/2,variables['infinity']/2,variables['infinity']/2]
view = [0,0,0]
Vradius = 300
particles = []
forces = {'G':{}}
structures = []
switches = {'viewChange':True,'followChange':False,'selectText':'track..'}
drawings = [{'type':'axes','center':Center,'size':(variables['infinity']/2)}]
def alpha():
    for force in forces:
        forces[force]['Constant'] = getC(force)
    for structure in structures:
        for subStruc in structure:
            for particle in structure[subStruc]:
                particles.append(particle)
    for t in range(0,variables['infinity']):
        time(t)
    display()
def time(t):
    for p in range(0,len(particles)):
        particles[p]['F'].append(Forces(particles[p],t))
        if t+1 < variables['infinity']:
            newVel = []
            newPos = []
            for i in range(0,variables['sDimensions']):
                newVel.append(particles[p]['V'][t][i]+(particles[p]['F'][t][i]/particles[p]['S']['G']))
                newPos.append(particles[p]['P'][t][i]+(newVel[i]*variables['fineStructure']))
            particles[p]['V'].append(newVel)
            particles[p]['P'].append(newPos)
def Forces(particle,t):
    force = 0
    fOrces = [0]*variables['sDimensions']
    for particl in particles:
        if particl != particle:
            for Force in forces:
                force += (forces[Force]['Constant']*particle['S'][Force]*particl['S'][Force])/RhoTB(particl['P'][t], particle['P'][t])[3];
            RTB = RhoTB(particle['P'][t], particl['P'][t])
            fOrces[0] += (RTB[5][0])*pow((pow(force*(math.cos(RTB[2])*math.sin(RTB[1])), 2)),0.5);
            fOrces[1] += (RTB[5][1])*pow((pow(force*(math.sin(RTB[2])*math.sin(RTB[1])), 2)),0.5);
            fOrces[2] += (RTB[5][2])*pow((pow(force*math.cos(RTB[1]), 2)),0.5);
    return fOrces
def RhoTB(A, B) :
    Ddelta = [0]*variables['sDimensions']
    DdeltaSigns = [0]*variables['sDimensions']
    for i in range(0,variables['sDimensions']):
        Ddelta[i] = (B[i])-(A[i]);
    RhoSQ = (pow(Ddelta[0], 2))+(pow(Ddelta[1], 2))+(pow(Ddelta[2], 2));
    for i in range(0,variables['sDimensions']):
        if Ddelta[i] != 0:
            DdeltaSigns[i] = (math.sqrt(pow(Ddelta[i], 2))/Ddelta[i])
        else :
            DdeltaSigns[i] = 0;
    DdeltaSignsTot = 1;
    for i in range(0,variables['sDimensions']):
        if DdeltaSigns[i] != 0 :
            DdeltaSignsTot *= DdeltaSigns[i];
    theta = math.acos(Ddelta[2]/(DdeltaSignsTot*math.sqrt(RhoSQ)));
    if Ddelta[1] != 0 :
        beta = math.atan(Ddelta[1]/Ddelta[0]);
    else :
        beta = 0;
    return [math.sqrt(RhoSQ), theta, beta, RhoSQ, Ddelta, DdeltaSigns]
def getC(force):
    #print C
    if force == 'G':
        k = 9.1093829140*pow(10,-31)
        K = 1#6.6738480*pow(10,-11)
    else:
        k = 0
        K = 0
    C = pow(k,2)*K
    return C
    #(1.60217656535×10−38)/4*3.14159265359*(8.85418781762×10−12)
def buildStructure(Type,center):
    if Type == 'stellar pair':
        structure = {}
        structure['planet'] = buildStructure('planet',[center[0]+150e6,center[1],center[2]])
        structure['star'] = buildStructure('star',center)
        return structure
    elif Type == 'star':
        particle = []
        particle.append({'S':{'G':1.98892e30},'P':[center],'V':[[0,0,pow(10,-31)]],'F':[]})
        return particle
    elif Type == 'planet':
        particle = []
        for p in range (0,1):
            particle.append({'S':{'G':5.9742e24},'P':[center],'V':[[0,28e3,0]],'F':[]})
            #particle.append({'EM':-1,'G':1,'WN':0,'SN':0,'P':[[center[0]+40,center[1]+10,center[2]+10]],'V':[[0,0.4*pow(10,-30),0]],'F':[]})
        return particle
#structures.append(buildStructure('atom',Center))
structures.append(buildStructure('stellar pair',[200,200,200]))
#structures.append(buildStructure('singularity',[250,250,250]))
def display():
    #print variables['view']
    syst.delete('all')
    canv.delete('all')
    tabl.delete('all')
    drawTimeScale()
    drawZoomScale()
    syst.create_text(45,5,text='t:   '+str(variables['t']),anchor=NW)
    syst.create_text(45,25,text='Th: '+str(int(view[0]*100/math.pi)/100.00),anchor=NW)
    syst.create_text(45,45,text='B:   '+str(int(view[1]*100/math.pi)/100.00),anchor=NW)
    follow = syst.create_rectangle(45,65,95,85,activefill='#88f')
    syst.create_text(45,85,text=switches['selectText'],anchor=NW)
    syst.tag_bind(follow,'<Button-1>',changeFollow)
    uniOut = syst.create_rectangle(45,205,95,225,activefill='#f88')
    printUniOut()
    if switches['followChange']:
        canv.create_rectangle(mouseP[0],mouseP[1],selectP[0],selectP[1])
    for p in range(0,len(particles)):
        global Center
        if variables['view'] == 'mid':
            Center = [variables['infinity']/2,variables['infinity']/2,variables['infinity']/2]
        elif p == variables['view']:
            Center = particles[p]['P'][variables['t']]
    for drawing in drawings:
        draw(drawing)
    for particle in particles:
        #if particle == particles[variables['view']]:
        if variables['t'] > 50:
            start = variables['t']-50
        else:
            start = 0
        for t in range (start,variables['t']):
            if t != 0:
                proj1 = projection(particle['P'][t])
                proj2 = projection(particle['P'][t-1])
                drawLine(proj1,proj2)
            if t+1 == (variables['t']):
                proj = projection(particle['P'][t])
                X = proj[0]
                Y = proj[1]
                draw({'type':'sphere','center':particle['P'][t],'size':0.25*pow(particle['S']['G'],(1/3.00))})
                draw({'type':'axes','center':particle['P'][t],'size':0.25*pow(particle['S']['G'],(1/3.00))})
                draw({'type':'3Darrow','center':particle['P'][t],'size':[particle['F'][t][0]*variables['fineStructure']*10,particle['F'][t][1]*variables['fineStructure']*10,particle['F'][t][2]*variables['fineStructure']*10]})
def drawTimeScale():
    divs = 10
    for div in range (0,divs):
        Y = (variables['infinity']/float(divs))*div
        syst.create_line(0,Y,10,Y)
        for d in range (0,divs):
            y = Y+(variables['infinity']/float(pow(divs,2)))*d
            syst.create_line(0,y,5,y)
        if Y == int(Y):
            Y=int(Y)
        syst.create_text(10,Y,text=Y,anchor=W)
    #syst.create_rectangle(20,variables['t']-5,40,variables['t']+5)
    syst.create_line(30,variables['t'],0,variables['t'])
    syst.create_line(30,variables['t'],40,variables['t']-5)
    syst.create_line(30,variables['t'],40,variables['t']+5)
    syst.create_line(40,0,40,variables['infinity'])
def drawZoomScale():
    syst.create_rectangle(40,100,70,200)
    Y = ((Vradius/500.00)*100)+100
    syst.create_line(40,Y,70,Y)
def draw (drawing):
    Type = drawing['type']
    cent = drawing['center']
    size = drawing['size']
    if Type == 'axes':
        point1 = projection([cent[0]+size,cent[1],cent[2]])
        point2 = projection([cent[0]-size,cent[1],cent[2]])
        point3 = projection([cent[0],cent[1]+size,cent[2]])
        point4 = projection([cent[0],cent[1]-size,cent[2]])
        point5 = projection([cent[0],cent[1],cent[2]+size])
        point6 = projection([cent[0],cent[1],cent[2]-size])
        drawLine(point1,point2)
        drawLine(point3,point4)
        drawLine(point5,point6)
    elif Type == '3Darrow':
        point1 = projection(cent)
        point2 = projection([cent[0]+size[0],cent[1]+size[1],cent[2]+size[2]])
        point3 = projection([cent[0],cent[1]+size[1],cent[2]+size[2]])
        point4 = projection([cent[0]+size[0],cent[1],cent[2]+size[2]])
        point5 = projection([cent[0]+size[0],cent[1]+size[1],cent[2]])
        drawLine(point1,point2)
        drawLine(point2,point3)
        drawLine(point2,point4)
        drawLine(point2,point5)
    elif Type == "sphere" :
        Sects = 10
        pnts = []
        for j in range(0,Sects):
            be = (j*math.pi)/Sects
            for i in range(0, Sects):
                th = (i*2*math.pi)/Sects
                pnts.append([cent[0]+(size*math.cos(th)*math.sin(be)),cent[1]+(size*math.sin(th)*math.sin(be)),cent[2]+(size*math.cos(be))])
        for p in range(0,len(pnts)):
            if p:
                drawLine(projection(pnts[p-1]),projection(pnts[p]))
            #else:
            #    drawLine(projection(pnts[len(pnts)-1]),projection(pnts[p]))
def drawLine(point1,point2):
    try:
        canv.create_line(point1[0],point1[1],point2[0],point2[1])
    except:
        pass
def projection(point, midpoint=0):
    fail = [None,None]
    if midpoint == 0:
        midpoint = Center
    Xd = point[0]-midpoint[0];
    Yd = point[1]-midpoint[1];
    Zd = point[2]-midpoint[2];
    Vwidth = (Xd*math.cos(view[1]))-(Zd*math.sin(view[1]));
    Vheight = (Yd*math.cos(view[0]))-(Xd*math.sin(view[1])*math.sin(view[0]))-(Zd*math.cos(view[1])*math.sin(view[0]));
    Vdepth = Vradius-((Yd*math.sin(view[0]))+(Xd*math.sin(view[1])*math.cos(view[0]))+(Zd*math.cos(view[1])*math.cos(view[0])));
    if Vdepth > 0:
        Vwidthzeta = (math.cos(view[2])*Vwidth)+(math.sin(view[2])*Vheight);
        Vheightzeta = (math.cos(view[2])*Vheight)-(math.sin(view[2])*Vwidth);
        projw = (variables['infinity']/2)+Vwidthzeta*((Vradius-variables['infinity'])/Vdepth);
        projh = (variables['infinity']/2)+Vheightzeta*((Vradius-variables['infinity'])/Vdepth);
        return [projw, projh]
    else:
        return fail
def printUniOut():
    row = 0
    colW = 40
    rowH = 20
    for p in range(0,len(particles)):
        col = 0
        tabl.create_rectangle(10+(colW*col),10+(rowH*row),10+(colW*(col+1)),10+(rowH*(row+1)))
        tabl.create_text(12+(colW*col),12+(rowH*row),text=p,anchor=NW)
        row+=1
        for stat in particles[p]['S']:
            colB = 0.6
            tabl.create_rectangle(10+(colW*col),10+(rowH*row),10+(colW*(col+colB)),10+(rowH*(row+1)))
            tabl.create_text(12+(colW*col),12+(rowH*row),text=stat,anchor=NW)
            col+=colB
            colB = 1
            tabl.create_rectangle(10+(colW*col),10+(rowH*row),10+(colW*(col+colB)),10+(rowH*(row+1)))
            tabl.create_text(12+(colW*col),12+(rowH*row),text=int(round(particles[p]['S'][stat],0)),anchor=NW)
            col+=colB
        col = 0
        row+=1
        for prop in particles[p]:
            if prop != 'S':
                colB = 0.3
                tabl.create_rectangle(10+(colW*col),10+(rowH*row),10+(colW*(col+colB)),10+(rowH*(row+1)))
                tabl.create_text(12+(colW*col),12+(rowH*row),text=prop,anchor=NW)
                col+=colB
                for d in particles[p][prop][variables['t']]:
                    colB = 2.2
                    if d < 1 and d > -1 and d != 0:
                        a = str(d).split('e')
                        s = str(round(float(a[0]),5))+'e'+str(int(a[1]))
                        #s=0
                    else:
                        s = str(round(d,5))
                    tabl.create_rectangle(10+(colW*col),10+(rowH*row),10+(colW*(col+colB)),10+(rowH*(row+1)),fill='#ffffff')
                    tabl.create_text(12+(colW*col),12+(rowH*row),text=s,anchor=NW)
                    col+=colB
                col = 0
                row+=1
        #print 'G',particle['G']
        #print 'EM',particle['EM']
        #for t in range(0,variables['t']):
        #    print t, particle['P'][t]
        #print 'G'+particle['G']
mouseP = [0,0]
selectP = [0,0]
def changeFollow(event):
    global mouseP,selectP
    switches['viewChange'] = False
    switches['followChange'] = True
    switches['selectText'] = 'select..'
    mouseP = [0,0]
    selectP = [0,0]
    #print event
def changeScale(event):
    if event.x < 40:
        tau = event.y
        if tau < variables['infinity'] and tau > 0:
            variables['t'] = tau
    elif event.x>40 and event.x<70 and event.y>100 and event.y<200:
        global Vradius
        Vradius = ((event.y-100)/100.00)*500
    display()
def change(event):
    global mouseP,selectP
    deltaP = [mouseP[0]-event.x,mouseP[1]-event.y]
    if switches['viewChange']:
        mouseP = [event.x,event.y]
        view[1] -= deltaP[0]/360.00
        view[0] -= deltaP[1]/360.00
    else:
        selectP = [event.x,event.y]
    display()
def start(event):
    global mouseP
    #if switches['viewChange']:
    mouseP = [event.x,event.y]
    if switches['followChange']:
        switches['selectText'] = 'select'
def stopSelect(event):
    global mouseP, selectP
    if switches['followChange']:
        for p in range(0,len(particles)):
            t = variables['t']
            P = particles[p]['P'][t]
            proj = projection(P)
            X = proj[0]
            Y = proj[1]
            if mouseP[0]<selectP[0]:
                X1 = mouseP[0]
                X2 = selectP[0]
            else:
                X2 = mouseP[0]
                X1 = selectP[0]
            if mouseP[1]<selectP[1]:
                Y1 = mouseP[1]
                Y2 = selectP[1]
            else:
                Y2 = mouseP[1]
                Y1 = selectP[1]
            if X>X1 and Y>Y1 and Y<Y2 and X<X2:
                variables['view'] = p
                switches['selectText'] = 'track '+str(p)
        switches['viewChange'] = True
        switches['followChange'] = False
        mouseP = [0,0]
        selectP = [0,0]
        display()
syst.bind('<B1-Motion>',changeScale)
canv.bind('<B1-Motion>',change)
canv.bind('<ButtonRelease-1>',stopSelect)
canv.bind('<Button-1>',start)
alpha()
