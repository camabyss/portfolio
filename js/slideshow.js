//This array keeps count of the image that each slide-show is currently showing
//Originally it was just a numerical value, but i made it an array to allow multiple slide-shows on one page.
img = [];
//This function searches the img array and returns the position of the image count variable
//as will be seen later, the slide-show stores the image count along with the id of the slide-show (at [1] and [0] respectively)
//this function uses the id as a reference to find the right number
function findImgVar (id){
	for (var i=0; i<img.length; i++){
		if (img[i][0] == id){
			return i;
		}
	}
}
//This function increments the image count and then displays the correct image by changing the source
//it also checks to see that the slide-show has not gone past the maximum, and if it has, goes back to the start
function nextImage (id,dir,pre,imgs,post) {
	v = findImgVar(id)
	e = document.getElementById(id)
	img[v][1]+=1;
	if (img[v][1]==imgs){
		img[v][1] = 0;
	}
	e.src = dir+pre+img[v][1]+post;
}
//this function is almost identical to nextImage, except it decrements the count first and checks to see if it's below 0 
function prevImage (id,dir,pre,imgs,post) {
	v = findImgVar(id)
	e = document.getElementById(id)
	img[v][1]-=1;
	if (img[v][1]<0){
		img[v][1] = imgs-1;
	}
	e.src = dir+pre+img[v][1]+post;
}
//This was intended to run the slide-show automatically, but I haven't used it since multiple slide-shows have been present
//I modified the function to carry over all the needed parameters for the next image function. The autoslide is now called from the initiation of each slide-show
//I tried a continuous automatic slide-show, but the timer had an error where it would double up on itself and go through the images too fast. As a compromise I simply added a timer for each of the images in the set, so that it flicks though them all and then stops when it gets back round to the start.
function autoSlide(id,dir,pre,imgs,post){
	for (var i = 0; i<imgs; i++){
		window.setTimeout("nextImage('"+id+"','"+dir+"','"+pre+"',"+imgs+",'"+post+"')",1000*(i+1))
	}
}
//Here is where the slide-shows are made, this is useful because it can be initiated from one line in the HTML, as opposed to setting up the parts each time.
//it uses the height and width to customise the size and to position the next/prev buttons dynamically.
//the buttons are given the nextImage and prevImage onClick
//the id, along with the initial image count (0) are added to the image count array
function makeSlideShow (pid,id,dir,pre,imgs,post,title,w,h) {
	e = $('#'+pid);
	e.width(w+48);
	e.html("<a id=ssprev href=javascript:void(0); onClick=prevImage('"+id+"','"+dir+"','"+pre+"',"+imgs+",'"+post+"') style=margin-top:"+((h/2)-20)+"px;> <img src=images/components/prev.png> </a> <img class=slideshow id="+id+" src="+dir+pre+'0'+post+" width="+w+" height="+h+"> <a id=ssnext href=javascript:void(0); onClick=nextImage('"+id+"','"+dir+"','"+pre+"',"+imgs+",'"+post+"') style=margin-top:"+((h/2)-20)+"px;> <img src=images/components/next.png > </a> <h2 class=desc>"+title+"</h2>");
	img.push([id,0])
	autoSlide(id,dir,pre,imgs,post);
}
//This was the initiation of the automatic slide-show
////autoSlide()
