function Desktop (settings) {
    settings = Def(settings,{
            parent:$('body')
        })
    this.size = [settings.parent.width(), settings.parent.height()]
    this.min_win_size = [300,300]
    this.animation_duration = 500
    this.minimal_size = [300,20]
    this.eDict = {}
    //Constructor
    var Build = function () {
        //if save cookie, get
        if (GC('save')) {
            save = GC('save')
        }
        Frames.call(this)
        Content.call(this)
        Actions.call(this)
    }
    //Construction
    var Frames = function () {
        ME(this.eDict,'desktop',settings.parent)
        ME(this.eDict,'windows',this.eDict.desktop)
        ME(this.eDict,'actions',this.eDict.desktop,['window'])
        ME(this.eDict,'indicators',this.eDict.desktop)
        ME(this.eDict,'messages',this.eDict.desktop)
    }
    var Content = function () {
        ME(this.eDict,'save',this.eDict.actions,'a',['btn'],
            {href:'javascript:void(0)'
            })
        this.eDict.save.html('save')
        if (save != false)
            this.eDict.save.addClass('selected')
        ME(this.eDict,'close',this.eDict.actions,'a',['btn'],
            {href:'javascript:void(0)'
            })
        this.eDict.close.html('close')
        for (i=0; i<parseInt((this.eDict.windows.width()-this.eDict.actions.width())/this.minimal_size[0]); i++) {
            docks.push(0)
        }
    }
    var Actions = function () {
        this.eDict.save.Click(AutoSave,this)
        this.eDict.close.Click(CloseAll,this)
    }
    //State saving (cookies)
    var save = false
    var AutoSave = function () {
        //set cookie
        if (save == false) {
            save = {}
            this.eDict.save.addClass('selected')
            for (w in windows) {
                windows[w].Save()
            }
            this.Message('Save ON')
        } else {
            save = false
            this.eDict.save.removeClass('selected')
            SC('save','',-1)
            this.Message('Save OFF')
        }
    }
    this.Save = function (title,settings) {
        if (save != false) {
            save[title] = settings
            SC('save',save,10)
        }
    }
    var CloseAll = function () {
	for (w in windows) {
	    windows[w].Close()
	}
    }
    this.BringUp = function (title) {
	for (w in windows) {
	    if (windows[w].title == title) {
		windows[w].BringUp()
	    }
	}
    }
    //Add window
    var windows = []
    this.Add = function (content,title,settings) {
        //if title saved, load open settings
        if (save != false && title in save) {
            for (s in save[title]) {
                settings[s] = save[title][s]
            }
        }
        windows.push(new Window (
            content,title,settings,
            this
            ))
    }
    //Notifications
    var message_count = 0
    this.Message = function (message) {
        switch (TO(message)) {
            case '{}':
                message = message.toSource()
                break
        }
        this.eDict.messages.append('<p id=message'+message_count+' >'+message+'</p>');
        window.setTimeout(
            "$('#message"+message_count+"')\
                .animate({opacity:0})\
                .animate({\
                    height:0,\
                    padding:0,\
                    margin:0\
                },function(){$(this).remove()})", 2500);
        message_count+=1;
    }
    //Dock management
    var docks = []
    this.Dock = function(wind) {
        d = this.NextDock()
        if (d >= 0) {
            docks[d] = wind
            return d
        } else {
            return false
        }
    }
    this.MoveDock = function (wind,i) {
        this.UnDock(wind)
        docks[i] = wind
    }
    this.UnDock = function (wind) {
        docks[docks.indexOf(wind)] = 0
    }
    this.NextDock = function () {
        return docks.indexOf(0)
    }
    this.EmptyDock = function (i) {
        return docks[i] == 0
    }
    //Shows avaliable docks
    this.Slots = function (wind) {
        //for each empty dock
        for (d in docks) {
            if (this.EmptyDock(d)) {
                //make faux panels and mins
                ME(this.eDict,'panel'+d,this.eDict.indicators,['indicator'],
                    {
                    width:this.min_win_size[0],
                    height:this.min_win_size[1],
                    left:this.min_win_size[0]*d,
                    top:this.size[1]-this.min_win_size[1]
                })
                ME(this.eDict,'minimal'+d,this.eDict.indicators,['indicator'],
                    {
                    width:this.minimal_size[0],
                    height:this.minimal_size[1],
                    left:this.min_win_size[0]*d,
                    top:this.size[1]-this.minimal_size[1]
                })
            }
        }
    }
    //Make sense of relative values and center keyword
    this.Relative = function (pos,size) {
        ret = []
        for (p in pos){
            //If the position is negative, make relative to the opposite side
            if (pos[p] < 0){
                ret[p] = pos[p]+(this.size[p]-size[p])
            //Makes sense of the center key word
            } else if (pos[p] == 'center' || pos[p] == 'centre'){
                ret[p] = (this.size[p]-size[p])/2;
            } else ret[p] = pos[p]
        }
        return ret
    }
    //Construct
    Build.call(this)
}
function Window (content,title,settings,desktop) {
    settings = Def(settings,{
            size:[400,400],
            position:['center','center'],
            state:'normal',
            icon:'images/content/oversword.gif',
            open:{},
            restore:false,
            current:{},
            panel:false,
            dock:false
        })
    this.title = title
    //Elements, by name
    var eDict = {}
    //Constructor
    var Build = function () {
        Icon.call(this)
        //Minimum sizing
        if (!('size' in settings.open)) {
            settings.open.size = []
            for (s in settings.size) {
                if (settings.size[s] < desktop.min_win_size[s]) {
                    settings.open.size[s] = desktop.min_win_size[s]
                } else {
                    settings.open.size[s] = settings.size[s]
                }
            }
        }
        //Relative positioning
        if (!('pos' in settings.open)) {
            settings.open.pos = desktop.Relative(settings.position,settings.open.size)
        }
        if (settings.save == undefined) {
            if (settings.state != 'none') {
                settings.open.state = settings.state
                this.Open()
            } else if (settings.open.state == undefined) {
                settings.open.state = 'normal'
                settings.current.state = 'none'
            }
        } else {
            if (settings.save.state != 'none') {
                settings.open = settings.save
                this.Open()
            } else {
                if (settings.open.state == undefined) {
                    settings.open.state = 'normal'
                }
                settings.current.state = 'none'
            }
        }
    }
    //Construction
    var Icon = function () {
        ME(eDict,'icon',desktop.eDict.desktop,'a',
           {href:'javascript:void(0)'})
        ME(eDict,'icon-image',eDict.icon,
           {backgroundImage:'url('+settings.icon+')'})
        ME(eDict,'icon-title',eDict.icon,'h2')
        eDict['icon-title'].html(title)
        eDict.icon.Click(this.Open,this)
    }
    var Frames = function () {
        ME(eDict,'window',desktop.eDict.windows,'article',
            {
                minWidth:desktop.min_win_size[0],
                minHeight:desktop.min_win_size[1],
                left:eDict.icon.offset().left,
                top:eDict.icon.offset().top,
                width:eDict.icon.width(),
                height:eDict.icon.height()
                //,opacity:0
            })
            ME(eDict,'top',eDict.window,'header')
                ME(eDict,'move',eDict.top,'a',
                    {href:'javascript:void(0)'
                    })
                ME(eDict,'sel',eDict.top,'a',
                    {href:'javascript:void(0)'
                    })
                ME(eDict,'min',eDict.top,'a',
                    {href:'javascript:void(0)'
                    })
                ME(eDict,'pan',eDict.top,'a',
                    {href:'javascript:void(0)'
                    })
                ME(eDict,'title',eDict.top,'h2')
                ME(eDict,'info',eDict.top,'h2')
            ME(eDict,'content',eDict.window,'section')
            //ME(eDict,'bottom',eDict.window,'footer')
            //    ME(eDict,'resize',eDict.bottom,//'a',
            //        {href:'javascript:void(0)'
            //        })
    }
    var Content = function () {
        eDict.move.css('background-image','url(images/components/move-grip.png)')
        eDict.sel.css('background-image','url(images/components/sel.png)')
        eDict.title.html(title)
        //eDict.Content = new Scroll(eDict.content)
        //alert(eDict.Content)
        //HL(eDict.Content.Add(),content)
        HL(eDict.content,content)
    }
    var Actions = function () {
        this.Save()
        eDict.window.mousedown(ToTop)
        eDict.sel.Click(Slots,this)
        switch (settings.current.state) {
            case 'normal':
                eDict.window
                .Drag({
                    containment:desktop.eDict.desktop,
                    handle:eDict.move,
                    stop:[SaveCurrent,this],
                    helper: function () {
                        return $(this)
                            .clone()
                            .empty()
                            .addClass('helper')
                            .removeClass('window')
                    }
                    })
                .Resize({
                    containment:desktop.eDict.desktop,
                    stop:[SaveCurrent,this],
                    helper: 'helper',
                    minWidth:desktop.min_win_size[0],
                    minHeight:desktop.min_win_size[1],
                    })
                eDict.min
                .Click(this.Minimal,this)
                .css('background-image','url(images/components/min.png)')
                eDict.pan
                .Click(this.Panel,this)
                .css('background-image','url(images/components/pan.png)')
                break
            case 'minimal':
                eDict.window
                .Drag({
                    axis:'x',
                    handle:eDict.move,
                    grid:desktop.minimal_size,
                    drag:[DockDrag,this],
                    stop:[DockRelease,this]
                    })
                .Resize('destroy')
                eDict.min
                .Click(this.Normal,this)
                .css('background-image','url(images/components/res.png)')
                eDict.pan
                .Click(this.Panel,this)
                .css('background-image','url(images/components/pan.png)')
                break
            case 'panel':
                //eDict.top.addClass('ui-resizable-handle ui-resizable-n')
                eDict.window
                .Drag({
                    axis:'x',
                    handle:eDict.move,
                    grid:desktop.minimal_size,
                    drag:[DockDrag,this],
                    stop:[DockRelease,this]
                    })
                .Resize({
                    handles:'n',
                    containment:desktop.eDict.desktop,
                    stop:[PanelHeight,this]
                    })
                eDict.min
                .Click(this.Normal,this)
                .css('background-image','url(images/components/res.png)')
                eDict.pan
                .Click(this.Minimal,this)
                .css('background-image','url(images/components/min.png)')
                break
            case 'none':
                break
        }
    }
    //Dock managing
    var DockDrag = function () {
        if (eDict.window.offset().left != settings.dock*desktop.minimal_size[0]) {
            ndock = eDict.window.offset().left/desktop.minimal_size[0]
            if (desktop.EmptyDock(ndock)) {
                settings.dock = ndock
                desktop.MoveDock(this,settings.dock)
                destop.Message(settings.dock)
            }
        }
    }
    var DockRelease = function () {
        eDict.window.animate({left:settings.dock*desktop.minimal_size[0]},desktop.animation_duration)
        this.Save()
    }
    var Dock = function () {
        if (settings.current.state == 'normal') {
            settings.restore = {size:settings.current.size,pos:settings.current.pos}
        }
        if (settings.dock == false) {
            settings.dock = desktop.Dock(this)
        }
        eDict.window.css({minHeight:desktop.minimal_size[1]})
    }
    var UnDock = function () {
        desktop.UnDock(this)
        settings.dock = false
    }
    //State saving
    var SaveCurrent = function () {
        drag = $('.helper')
        eDict.window.offset(drag.offset())
        eDict.window.width(drag.width())
        eDict.window.height(drag.height())
        settings.current.pos = [eDict.window.offset().left,eDict.window.offset().top]
        settings.current.size = [eDict.window.width(),eDict.window.height()]
        this.Save()
    }
    var PanelHeight = function () {
        if (eDict.window.height() <= desktop.minimal_size[1]*2) {
            this.Minimal()
        } else {
            settings.panel = eDict.window.height()
            this.Save()
        }
    }
    //Show and hide avaliable slots
    var Slots = function () {
        if ($('.indicator').length == 0) {
            if (settings.current.state != 'normal') {
                ME(eDict,'restore',desktop.eDict.indicators,['indicator'],
                    {
                    width:settings.restore.size[0],
                    height:settings.restore.size[1],
                    left:settings.restore.pos[0],
                    top:settings.restore.pos[1]
                })
            }
            desktop.Slots(this)
            $('.indicator').Click(UnSlots,this)
        } else {
            desktop.eDict.indicators.empty()
        }
    }
    var UnSlots = function () {
        action = $('.indicator:hover').attr('class').split(' ')[0]
        if (action == 'restore') {
            this.Normal()
        } else {
            settings.dock = parseInt(action.slice(-1))
            type = action.slice(0,-1)
            if (type != settings.current.state) {
                if (type == 'panel') {
                    this.Panel()
                } else if (type == 'minimal') {
                    this.Minimal()
                }
            }
            eDict.window.animate({left:settings.dock*desktop.minimal_size[0]},desktop.animation_duration)
            desktop.MoveDock(this,settings.dock)
        }
        desktop.eDict.indicators.empty()
    }
    //Layering windows
    var ToTop = function () {s
        desktop.eDict.windows.append(
            eDict.window.detach()
        )
    }
    this.Save = function () {
        desktop.Save(title,{
            save:settings.current,
            restore:settings.restore,
            panel:settings.panel,
            dock:settings.dock})
    }
    //State changes
    this.BringUp = function () {
	if (settings.current.state == 'none') {
	    this.Open()
	} else if (settings.current.state == 'minimal') {
	    this.Panel()
	}
	ToTop()
    }
    this.Normal = function () {
        if (settings.current.state == 'minimal' || settings.current.state == 'panel') {
            UnDock.call(this)
        }
        if (settings.restore == false || settings.current.state == 'none') {
            settings.restore = {size:settings.open.size,pos:settings.open.pos}
        }
        eDict.window.css({minHeight:desktop.min_win_size[1]})
        eDict.window.animate({
            width:settings.restore.size[0],
            height:settings.restore.size[1],
            left:settings.restore.pos[0],
            top:settings.restore.pos[1]
        },desktop.animation_duration,Call(SaveCurrent,this))
        settings.current.state = 'normal'
        Actions.call(this)
    }
    this.Minimal = function () {
        if (desktop.NextDock() != -1) {
        if (settings.current.state != 'panel') {
            Dock.call(this)
        }
        eDict.window.animate({
            width:desktop.minimal_size[0],
            left:settings.dock*desktop.minimal_size[0],
            height:desktop.minimal_size[1],
            top:desktop.size[1]-desktop.minimal_size[1]
        },desktop.animation_duration)
        settings.current.state = 'minimal'
        Actions.call(this)
        } else desktop.Message('No docks avaliable')
    }
    this.Panel = function () {
        if (desktop.NextDock() != -1) {
        if (settings.current.state != 'minimal') {
            Dock.call(this)
        }
        if (settings.panel == false) {
            settings.panel = desktop.min_win_size[1]
        }
        eDict.window.animate({
            width:desktop.minimal_size[0],
            left:settings.dock*desktop.minimal_size[0],
            height:settings.panel,
            top:desktop.size[1]-settings.panel
        },desktop.animation_duration)
        settings.current.state = 'panel'
        Actions.call(this)
        } else desktop.Message('No docks avaliable')
    }
    this.Close = function () {
	if (settings.current.state != 'none') {
        //remove icon class
        eDict.icon.removeClass('open')
        eDict.icon.Click(this.Open,this)
        //remember state &| position
        settings.open = Copy(settings.current)
        //remove window
        eDict.window.remove()
        if (settings.current.state != 'normal') {
            UnDock.call(this)
        }
        settings.current.state = 'none'
	}
    }
    this.Open = function () {
        eDict.icon.addClass('open')
        eDict.icon.Click(this.Close,this)
        Frames.call(this)
        Content.call(this)
        if (settings.open.state == 'panel') {
            this.Panel()
        } else if (settings.open.state == 'minimal') {
            this.Minimal()
        } else if (settings.open.state == 'normal') {
            this.Normal()
        }
    }
    //Construct
    Build.call(this)
}