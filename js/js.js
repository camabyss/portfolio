function Alert (obj) {
    alert(obj.toSource())
}
function Copy (obj) {
    ret = {}
    for (o in obj) {
        if (TO(obj[o]) == '{}') {
            ret[o] = Copy(obj[o])
        } else {
            ret[o] = obj[o]
        }
    }
    return ret
}
//Modified from http://www.w3schools.com/js/js_cookies.asp
function SC(name,value,expire) {
    value = JSON.stringify(value)
    var exdate=new Date();
    if (expire == undefined) {
        expire = 7
    }
    exdate.setDate(exdate.getDate() + expire);
    var c_value=escape(value) + ((expire==null) ? "" : "; expires="+exdate.toUTCString());
    document.cookie=name + "=" + c_value;
}
function GC(name) {
    var i,x,y,ARRcookies=document.cookie.split(";");
    for (i=0;i<ARRcookies.length;i++) {
        x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
        y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
        x=x.replace(/^\s+|\s+$/g,"");
        if (x==name) {
            return eval(unescape(y));
        }
    }
    return false
}
(function(){
        var interval;
        jQuery.event.special.contentchange = {
        setup: function(){
            var self = this,
            $this = $(this),
            $originalContent = $this.text();
            interval = setInterval(function(){
                if($originalContent != $this.text()) {
                        $originalContent = $this.text();
                    jQuery.event.handle.call(self, {type:'contentchange'});
                }
            },200);
        },
        teardown: function(){
            clearInterval(interval);
        }
    };

})();
(function($){
    $.fn.Click = function(handler,scope,dbl) {
        if (dbl) {
            this.unbind('dblclick')
            return this.dblclick (
                function (ev) {
                    if (TO(handler) == '[]') {
                        for (h in handler) {
                            handler[h].call(scope,ev)
                        }
                    } else {
                        handler.call(scope,ev)
                    }
                }
            )
        } else {
            this.unbind('click')
            return this.click (
                function (ev) {
                    if (TO(handler) == '[]') {
                        for (h in handler) {
                            handler[h].call(scope,ev)
                        }
                    } else {
                        handler.call(scope,ev)
                    }
                }
            )
        }
    };
})(jQuery);
(function($){
    $.fn.Drag = function(settings) {
        if (this.data('draggable')) {
            this.draggable('destroy')
        }
        var functs = {}
        for (s in settings) {
            if (TO(settings[s]) == '[]' && TO(settings[s][0]) == '()') {
                functs[s] = settings[s]
                settings[s] = Call(functs[s][0],functs[s][1])
            }
        }
        return this.draggable(settings)
    };
})(jQuery);
(function($){
    $.fn.Resize = function(settings) {
        if (this.data('resizable')) {
            this.resizable('destroy')
        }
        if (settings != 'destroy') {
            var functs = {}
            for (s in settings) {
                if (TO(settings[s]) == '[]' && TO(settings[s][0]) == '()') {
                    functs[s] = settings[s]
                    settings[s] = Call(functs[s][0],functs[s][1])
                }
            }
            return this.resizable(settings)
        }
    };
})(jQuery);
(function($) {
    $.fn.textfill = function(options) {
        if (options == undefined || options.maxFontPixels == undefined) {
            var fontSize = 25
        } else {
            var fontSize = options.maxFontPixels;
        }
        var ourText = $('span:visible:first', this);
        var maxHeight = $(this).height();
        var maxWidth = $(this).width();
        var textHeight;
        var textWidth;
        do {
            ourText.css('font-size', fontSize);
            textHeight = ourText.height();
            textWidth = ourText.width();
            fontSize = fontSize - 1;
        } while ((textHeight > maxHeight || textWidth > maxWidth) && fontSize > 3);
        return this;
    }
})(jQuery);
/*List of settings that are set with .attr
 *as opposed to .css or .width/height
 *Used in ME, Apply Settings
 */
attributes = ['href','src','id','alt']
/*TO ~ Type Of
 *Custom typeof function, returns a 2-char string denoting the objects type
 *This function can also determine between arrays and dictionaries
 *Can be extended to recognise custom classes
 */
function Call (funct,scope){
    return function () {funct.call(scope)}
}
function TO (object) {
    switch (typeof object) {
        case 'object':
            if (object instanceof Window) {
                return 'window'
            } else
            if (object instanceof Array) {
                return '[]'
            } else {
                return '{}'
            }
        case 'string':
            return "''"
        case 'boolean':
            return '01'
        case 'function':
            return '()'
        default:
            return ''
    }
}
/*ME ~ Make Element
 *creates and appends a jQuery element to an element dictionary
 *if no dictionary is defined, the function simply returns the element
 *parent can be string(selector) or jQuery object
 *name is the key in the dictionary, element is also given this as a class
 *other options:
 *  settings
 *  tag
 *  extra classes
 *  buildNow, which, if false, will allow the element to be appended later
 */
function ME (eDict,name,parent) {
    //Defaults and options
    tag = 'div'
    classes = []
    settings = {}
    buildNow = true
    if (TO(parent) == "''") {
        //parent = $(parent)
    }
    for (var a=3;a<arguments.length;a++){
	switch(TO(arguments[a])) {
	    case "''":
		tag = arguments[a]
		break;
	    case '[]':
		classes = arguments[a]
		break;
	    case '{}':
		settings = arguments[a]
		break;
	    case '01':
		buildNow = arguments[a]
		break;
	}
    }
    //Make object, set classes
    if (eDict != false && eDict != undefined && eDict != null) {
	obj = eDict[name] =
	    $('<'+tag+'/>',
		{
		    'class':name+' '+classes.join(' ')
		}
	    )
    } else {
	obj =
	    $('<'+tag+'/>',
		{
		    'class':name+' '+classes.join(' ')
		}
	    )
    }
    //Apply settings
    for (s in settings) {
	if (attributes.indexOf(s) != -1) {
	    obj.attr(s,settings[s])
	}
	else if (s == 'width') {
	    obj.width(settings[s])
	}
	else if (s == 'height') {
	    obj.height(settings[s])
	}
	else {
	    obj.css(s,settings[s])
	}
    }
    //Build and return
    if (buildNow != false) {
	parent.append(obj)
    }
    return obj
}
/*Uses jQuery's .click
 *but arranges functions so that 'this' can be refferenced
 *inside the handle function without confusing it with the
 *function itself
 */
function Click(element,funct,dblClck){
	dblClck = typeof dblClck !== 'undefined' ? dblClck : false;
	if (dblClck) {
		element.dblclick(
			function(){
				for (f in funct) {
					if (funct[f][1] == '' || funct[f][1] == 0 || funct[f].length == 1){
						funct[f][0]()
					} else {
						funct[f][0](funct[f][1])
					}
				}
			}
		)
	} else {
        //.not(children())
		element.click(
			function(event){
                //Excludes children from interaction http://www.kidsil.net/2011/05/jquery-click-excluding-child/
                if (event.target == this) {
                    for (f in funct) {
                        if (funct[f][1] == '' || funct[f][1] == 0 || funct[f].length == 1){
                            funct[f][0]()
                        } else {
                            funct[f][0](funct[f][1])
                        }
                    }
                }
			}
		)
	}
}
function indexOfDict (key,value,list) {
	for (l in list) {
		if (list[l][key] == value) {
			return l
		}
	}
	return -1
}
function HL (obj,file) {
    if (TO(obj) == "''") {
        obj = $(obj)
    }
	var txtFile = new XMLHttpRequest();
	txtFile.open("GET", file, true);
	txtFile.onreadystatechange = function() {
		if (txtFile.readyState === 4) {  // Makes sure the document is ready to parse.
			allText = txtFile.responseText
            if (TO(obj) == "''") {
                obj = $(obj)
            }
			obj.html(allText);
		}
	}
	txtFile.send(null);
}
function Def(properties,default_props){
	if (properties == null){
		properties = default_props
	} else {
		for (prop in default_props) {
			if (! (prop in properties)){
				properties[prop] = default_props[prop]
			}
		}
	}
	return properties;
}
function Fill(obj) {
    if (TO(obj) == "''") {
        obj = $(obj)
    }
	obj.width(window.innerWidth)
	obj.height(window.innerHeight)
	obj.resize(function () {
		obj.width(window.innerWidth)
		obj.height(window.innerHeight)
	})
}
function switchSelect(parid) {
	curr = $('#'+parid+" > #selected");
	curr.removeAttr('id');
	$(document.activeElement).attr('id','selected');
}
function setPicture(id,sorc){
	e = document.getElementById(id);
	e.src = 'images/content/'+sorc;
}